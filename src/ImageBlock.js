import { LazyLoadImage } from "react-lazy-load-image-component";

function ImageBlock(props) {
    return (
            <LazyLoadImage
                key={props.index}
                src={props.src}
                alt="Image not found"
                className="img-lazy col-6 col-md-4 img-thumbnail"
            />
        )
        ;
}


export default ImageBlock;
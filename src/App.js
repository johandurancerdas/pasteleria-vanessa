import data from './data.json';
import './App.css';
import './Image.css';
import ImageBlock from './ImageBlock';

function App() {
  return (
    <div>
      <div className="container">
        <div className="row">
          {
            data.map((record, i) => (
              <ImageBlock key={i} src={record.src} description={record.description} index={record.index}></ImageBlock>
            ))
          }
        </div>
      </div>
    </div>
  )
}

export default App;
